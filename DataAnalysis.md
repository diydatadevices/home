# Analyzing Your Data

## What Graph?

First, you need to decide what kind of graph will best fit your data.

* If your predictor variable (independent variable) is continuous, you need a line plot or scatter plot.
  * Use a line graph if your response can have only one measurement per unit of the predictor (for example, humidity over time)
  * Use a scatter plot if your response can potentially have several measurements per unit of the predictor (for example, hand temperature over age)


![](/Worksheets/img/AgeVsHandTemp.png)


* If your predictor variable (independent variable) is categorical, a bar chart is best.


![](/Worksheets/img/HandTempVsGender.png)



## Time as a Variable

Many of you will have graphs showing how a variable changes over time. For this you should use a line graph. But first, we need to convert the output of the millis() function into a usable format.

* You should have a column with the output of the millis() function. If you haven't already, convert this to seconds.
* Create a new blank row underneath the title of your variables.
* Create a column with the start time and date at the top in your blank row (e.g., 7/19/2017 5:00 PM)
* Fill in the rest of the column by entering this formula in your cell:
  * =$A$2 + time(0,0,B2)
  * A2 is the cell with your start time
  * B2 is the first cell in your seconds column
* Click on the box in the bottom right of this cell and drag down to fill in the rest of the column.
* Now you have a meaningful time variable!

## Making Your Graph

Line or Scatter Plot
* Highlight the two columns with the variables you want to graph. (By default, Excel puts the left column as the X-axis and the right column as the Y-axis. You can switch them if need be after making the graph)
* Click Insert
* Under Charts, choose the appropriate graph (line, scatter, bar)
* If you need to switch your X and Y axes, right-click (or cmd-click) on the graph > Select Data > Switch Row/Column.

Bar Chart
* First, calculate the average for each of your categories. You will basically make a new, smaller table like the one below. You can do this in the same spreadsheet alongside your data.
  * For example, calculate the average hand temperature for males and females separately.
  * Calculate the average by entering the formula =average(A2:A15) where A2:A15 contain the numbers you want to average.



| Gender | Av. Temperature |
| ------ | ------ |
| M | 80.5 |
| F | 82.1 |




## Making Your Graph Make Sense

Now you need to make sure someone can read it without you having to explain it. Your graph should have:
* A title
* Axes titles (with units!)
* A legend (if you are graphing multiple lines)

You can add all these elements by clicking the big green "+" next to the chart.

## Getting Fancy! (Optional)

Adding statistics to your graph can help determine if you've found a powerful effect, a weak effect, or no effect. This is easiest to do with a scatter plot or bar chart.

Scatter Plot
* Click the green + > Trendline > More options > then check both "Display Equation" and "Show R-squared"
  * An R2 of >0.5 is generally a strong effect in ecology and environmental science.
  * An R2 of 0.2 - 0.5 is a weak effect
  * An R2 of <0.2 may indicate a very weak or no relationship.


![](/Worksheets/img/HandTempVsAgeTrendline.png)


Bar Chart
* Calculate the standard error. Let me know if you need to do this and I'll demonstrate.


| Gender | Av. Temperature | SE |
| ------ | ------ | ------ |
| M | 80.6 | 6.2 |
| F | 81.8 | 9.1 |


In some cases you will want to show the output of multiple sensors over time. To do this, you should right-click the chart > Select Data > Add Series.
* Name the new series ("Humidity")
* Your x-values should stay the same as your original data. Highlight the time column.
* Your y-values will be the column with your new sensor data.
* Make sure to add a legend so we know which sensor is which!


![](/Worksheets/img/Age_Room_HandTemp.png)


# You can also change the shape, width, and color of your symbols and lines. Play with these options!


# In R
```

setwd("C:/Users/User/Desktop/ASRA/GitHub/notes") # point R to the folder that contains your file

df <- read.csv("Age_HandTempData.csv") # read in your data file
names(df) <- c("Age","Temp","Gender","Health", "RoomTemp") # rename your variables if necessary

plot(Temp ~ Age, data = df) # R will automatically plot the correct plot (scatter, bar chart) for your variable
# other graphical options:
#plot(....., pch = 19) #change symbol type
#plot(....., cex = 1.5) #change symbol size
#plot(....., col = "red") #change symbol color
#plot(....., xlim = c(0,100)) #change the x-axis to only show values between 0 and 100
#plot(....., ylim = c(0,100)) #change the y-axis to only show values between 0 and 100

# To plot multiple y-axes on the same graph
par(mar = c(5,5,2,5)) # Change the size of the graph so there's room on the right for your second y-axis
plot(Temp ~ Age, pch = 19, data = df) # plot your first set of data
par(new = T) #tells R to keep building on the same plot
plot(RoomTemp~Age, pch = 19, col = "red",data = df,axes=F, xlab=NA, ylab=NA) # plot your second set of data
axis(side = 4) # put an axis on the right side
mtext(side = 4, line = 3, 'RoomTemp') # Name your new axis

#add a legend
legend("topleft",
       legend=c("HandTemp","RoomTemp"),
       pch=c(19, 19), col=c("black", "red"))
```
