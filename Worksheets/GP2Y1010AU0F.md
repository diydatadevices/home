# Sharp Dust Sensor

## Reading:
* [tutorial](http://arduinodev.woofex.net/2012/12/01/standalone-sharp-dust-sensor/)
* [link to paper compairson paper](LINK TO PAPER)

## Hookup

| Sensor # | Color | Function | Connect to |
| ----: | :---- | :---:| :---: |
| 1 | White | V-LED | 150ohm R -> Vcc |
| 2 | Blue | GND-LED | GND |
| 3 | Green | LED | Digital Pin |
| 4 | Yellow | Sig-GND | GND |
| 5 | Black | Sig-OUT | Analog Pin |
| 6 | Red | Vcc | Vcc |

![](/Worksheets/img/IMG_20170712_141958.jpg)
