# Analog to Digital Conversion

## Summary
There are fundemental differences between the Analog world and the Digital world. This workshop will teach how the binary number system works and how to bring analog voltages into the digital world.


## Learning Goals
* Count in Binary
* Convert Decimal Numbers to Binary Numbers
* Convert Binary Numbers to Decimal Numbers
* What is an Analog to Digital Converter
* Learn how an Analog to Digital Converter works
* Reference Voltage
* The fundemental differences between Analog and Digital
*


## Materials
* 4 Notecards with a 0 written on one side, and a 1 on the other
* Sticky Notes
* Sharpie
* White-board and pens

## Notes

Start with the hands on activity explaining the binary number system. Make sure everyone is confident in doing conversions between Binary and Decimal.

Introduce Analog and Digital.

Discuss what some definitions might be.

> Analog is a ramp, digital is stairs.

> Analog: infinite resolution

> Digital: finite resolution

 Introduce the Analog to Digital Converter as a concept that converts an Infinite value in a Finite value.

 __This is accomplished by creating edges and walls around the infinite value__

* Reference Voltage, the upper limit of measureability.
* The Least Significant Bit, the smallest amount of detectable change
* Resolution, how many steps there are between the upper and lower limit of measureability.

The tie between Binary and ADC is the concept of the Least Significant Bit. The span between the reference voltage and ground is divided into a number of divisions dictated by the resolution (bit depth) of the ADC, which is effectivly the sensitivity of the device.

> In a 3 bit ADC, how many possible states are there? (Hint, same as a 3 bit Binary Number) In a 16 bit ADC, How many possible states are there?

```math
states = 2^N
```

N = Number of Bits


### What if we want to calculate the Least Significant Bit of an ADC?

When we break that ramp up into stairs, the Least Significant Bit is the height of each stair.

> What is the smallest amount of detectable change, if the reference voltage is 1v and we're using a 3bit ADC?

```math
 LSB = Vref/2^N
 ```
 > What is the LSB of a 10 bit ADC with a Vref of 3.3v? What about a 16 bit ADC with a Vref of 2v?

### If we want to calculate the output code of voltage input to an ADC?

```math
 Code = Vin/LSB
 ```

 > What would be the output code of a 2v input to a 10bit ADC with a reference Voltage of 3.3v? What about a 0.9243v input to a 16 bit ADC with a reference voltage of 2v?

 > How much does the Output Code change with a 4bit ADC with a Vref of 2v if the voltage changes by 0.001v? What about a 24bit ADC?

### If we want to calculate the voltage input from the Output code of the ADC?

```math
 Vin = Code * LSB
 ```

 > What would be the voltage if we got a value of 512 from a 10bit ADC with a reference voltage of 5v? What about 23287 from a 16 bit ADC with a reference of 2v?
