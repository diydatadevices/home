# Digital Sensor

## Reading

* [I2C](https://learn.sparkfun.com/tutorials/i2c)
* [BMP180 Documentation](https://www.sparkfun.com/products/retired/13848)
* [Si7021](https://www.sparkfun.com/products/13763)
* [Luminosity](https://www.sparkfun.com/products/12055)


## Hookup

Make sure power connections are powering breadboard like in the following image:

![](/Worksheets/img/asra-115.jpg)

Connect the sensor as follow:

| Arduino | Sensor | Function |
| ------: | :----- | :----: |
|  Digital Pin 2 | SDA or DA | Serial Data |
|  Digital Pin 3 | SCL or CL | Serial Clock |
|  3.3v | 3v3 or + | Power |
|  GND   | GND or - | Ground |

Here is an Image of the BMP180 correctly attached. Note the power connections and we're using different colors for the Dat and Clock Pins

![](/Worksheets/img/asra-106.jpg)

## BMP180 Example Code

Instead of having to do math and conversions on an analog signals, digital sensors take care of a lot of that for us. Here quick sketch to get you started with these sensors


```cpp
//For the BMP180 Barometric pressure sensor
#include <SFE_BMP180.h> //include sensor library
#include <Wire.h> //this is the library for using i2c communication
SFE_BMP180 pressure; //create a sensor object to interact with
//Create variables to hold data
char status;
double temp, pres, seaLevel, alt;
float ALTITUDE = 259.0; //Altitude of Fairbanks, AK in meters. We need this

void setup() {
  Serial.begin(9600); //begin serial
  while(!Serial); //wait until we open a serial monitor to start running code
  Serial.println("---Barometric Pressure Sensor Example---"); //print this message to the console
  if (pressure.begin()) { //initialize the sensor. This resolves as TRUE if the sensor is detected
    Serial.println("BMP180 Initialized");
  } else Serial.println("Sensor Failed to Initialize"); //if something is wrong, it will resolve as false
}

void loop() {
  //This nest of functions gets all data from the sensor sequentially
  status = pressure.startTemperature();
  if (status != 0) {
    delay(status);
    status = pressure.getTemperature(temp);
    if (status != 0) {
      Serial.println("got temp");
      delay(status);
      status = pressure.startPressure(3);
      if (status != 0) {
        delay(status);
        status = pressure.getPressure(pres, temp);
        if (status != 0) {
          Serial.println("got pressure");
          seaLevel = pressure.sealevel(pres, ALTITUDE); // we're at 1655 meters (Boulder, CO)
          alt = pressure.altitude(pres, seaLevel);
        }
      }
    }
  }
}
//Then we print out the values to the Serial Monitor
Serial.println();
Serial.print("temp ");
Serial.println(temp);
Serial.print("absoulte pressure ");
Serial.println(pres);
Serial.print("Sea Level ");
Serial.println(seaLevel);
Serial.print("altitude ");
Serial.println(alt);
delay(5000); //And wait some time before reading the sensor again

```

## Using Two sensors

The really cool thing about using digital sensors is that they all communicate over the same two wires, so we can connect multiple sensors in series as pictured below.

![](/Worksheets/img/asra-105.jpg)

## Example Code

Here is an example for the Si7021 Temperature and Relative Humidity sensor

```cpp
#include "SparkFun_Si7021_Breakout_Library.h" //This is the sensor Library
#include <Wire.h> //Again, the i2c Library
Weather tANDh; //in this case, we create an instance called 'tANDh'. It can be called anything
//and create some variables to hold values
float humidity = 0;
float temperature = 0;

void setup() {
  Serial.begin(9600); //begin serial
  while(!Serial); //wait until we open a serial monitor to start running code
  Serial.println("---Temperature and Humidity Sensor Example---"); //print this message to the console
  tANDh.begin(); //initialize and start the sensor
  Serial.println("Si7021 Initialized");
}

void loop() {
  humidity = sensor.getRH(); //get the reletive Humdidity
  temperature = sensor.getTempF(); //get the temperature in Farenheit
  Serial.print("Temp: ");
  Serial.print(temperature);
  Serial.print("F, ");

  Serial.print("Humidity: ");
  Serial.print(humidity);
  Serial.println("%");
}
```
This code for just the Temp/RH sensor.
The important thing to remember here is that the structure for these sensors is the same, but there will be subtle difference in getting data from each one. Just look at the documentation and try to puzzle it out.

#### Can you combine code from multiple sensors to print out their readings in the Serial Monitor at the same time?
