# Analog Sensor

## Reading
Make sure you are familiar with [sensors](https://itp.nyu.edu/physcomp/lessons/sensors-the-basics/) and [Ohms Law](https://vimeo.com/76442432) before starting on this.


# Lets Make A Graphite Resistor BEFORE DOING ANYTHING ELSE!

## Testing Ohms Law

We're going to be doing our first Analog Read on the Arduino using a potentiometer.

![](/Worksheets/img/asra-1.jpg)

![](/Worksheets/img/pot.gif)

### Code

```cpp
int analog_pin = A0; //The Pin to be Read
int analog_value;

void setup() {
  Serial.begin(9600); //Begin Serial
  while(!Serial);
  Serial.println("---Analog Input Example---");
}

void loop() {
  analog_value = analogRead(analog_pin); //Read Pin Value
  Serial.println(analog_value); //Print Value
  delay(100); //Wait a little bit
}
```

Upload the code, open the Serial Monitor, and make sure some values are printing out.

Turn the Potentiometer. What happens?

#### Which turning direction makes the values go up?

#### How would you reverse the values?

## The First Sensor

> In this tutorial we will be building a simple analog sensor that measures temperature.

This sensor is called a Thermistor, which is a resistor that changes resistance based on temperature.

[Adafruit](https://learn.adafruit.com/thermistor?view=all) has an informative tutorial about thermistory.

![](/Worksheets/img/asra-3.jpg)

![](/Worksheets/img/asra-109.jpg)


We will be using a 10k resistor, a 10k thermistor, and a jumper wire. The thermistor is connected between the 3.3v Power Bus and any empty row on the breadboard (in this case, row 44). The resistor is connected between GND and that same row (Row 44). Then Row 44 is connected to Pin A0 on the Arduino.

### Code

```cpp
int analog_pin = A0; //The Pin to be Read
int analog_value;

void setup() {
  Serial.begin(9600); //Begin Serial
  while(!Serial);
  Serial.println("---Analog Input Example---");
}

void loop() {
  analog_value = analogRead(analog_pin); //Read Pin Value
  Serial.println(analog_value); //Print Value
  delay(100); //Wait a little bit
}
```

Upload this code, open the Serial Monitor, and make sure you're getting values printing out.

Try touching the Thermistor. How do the values change?

### Does the resistance of the Thermistor increase or decrease as the temperature increases?

### How do you know?

## Converting to Temperature

To convert this meaningless number into an actual temperature we will use a library. This can be also be done using some math and something called the [Steinhart Equation](https://en.wikipedia.org/wiki/Thermistor#B_or_.CE.B2_parameter_equation), which converts the resistance of the thermistor to temperature based on characteristics of the thermistor which are documented in the [datasheet](http://cdn.sparkfun.com/datasheets/Sensors/Temp/ntcle100.pdf).

However, there is a simpler way. Someone wrote a library that does the conversion and the math for us.

```cpp
#include <Thermistor.h>

#define RREF 10000.0 //actual resistance of the fixed resistor

Thermistor therm(0); //the Analog Input pin that the Thermistor is connected to

float temp; //variable to hold the temperature

void setup() {
  Serial.begin(9600);
}

void loop() {
  temp = therm.getTemp(); //does analogRead and resistance -> temp conversions
  Serial.print("Temp: ");
  Serial.print(temp);
  Serial.println("C");
  delay(100);
}
```

The Library is included at the first line with the _#include_ tag. _#define RREF_ is the actual resistance value of the reference resistor, for more accurate temperature measurements, measure the actual resistance of that component and put in the value here.

The function _.getTemp()_ does the analogRead and the temperature conversion and returns the temperature in Celcius.


### Challenge: Can you Calibrate this sensor? How would you do that?

_[hint, ice water](https://learn.adafruit.com/calibrating-sensors/why-calibrate)_
