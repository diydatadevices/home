# Begining

## What is Electricity?
* [What is Electricity](https://learn.sparkfun.com/tutorials/what-is-electricity)
* [Electric Power](https://learn.sparkfun.com/tutorials/electric-power)
* [Ohms Law](https://learn.sparkfun.com/tutorials/voltage-current-resistance-and-ohms-law)

## Basic Electrical Components
* [Ohms Law Video (itp)](https://vimeo.com/76442432)
* [Resistivity Video (itp)](https://vimeo.com/76442431)
* [Electricity Basics (itp)](https://itp.nyu.edu/physcomp/lessons/electronics/electricity-the-basics/)

## What is a Microcontroller?
* [Microcontroller Basics](https://itp.nyu.edu/physcomp/lessons/microcontrollers/microcontrollers-the-basics/)

## What is a Breadboard?
* [How to Use a Breadboard](https://learn.sparkfun.com/tutorials/how-to-use-a-breadboard)

## What is a Sensor?
* [Sensors(itp)](https://itp.nyu.edu/physcomp/lessons/sensors-the-basics/)

## What is Programming?
* [Variables (itp)](https://itp.nyu.edu/physcomp/lessons/programming/variables/)
* [Programing Terms (itp)](https://itp.nyu.edu/physcomp/lessons/programming/programming-terms-and-programming-environments/)



## Extra Credit
* [LED](https://learn.sparkfun.com/tutorials/light-emitting-diodes-leds)
* [Capacitor](https://learn.sparkfun.com/tutorials/capacitors)
* [Diodes](https://learn.sparkfun.com/tutorials/diodes)
* [Integrated Circuits](https://learn.sparkfun.com/tutorials/integrated-circuits)
* [Resistors](https://learn.sparkfun.com/tutorials/resistors)
* [Battery Types](https://learn.sparkfun.com/tutorials/battery-technologies)
* [What is a Battery](https://learn.sparkfun.com/tutorials/what-is-a-battery)
