# Heat Island Project

## Reading

* [I2C](https://learn.sparkfun.com/tutorials/i2c)
* [Si7021](https://www.sparkfun.com/products/13763)
* [Luminosity](https://www.sparkfun.com/products/12055)
* [TEMT6000 - Ambient Light Sensor](https://www.sparkfun.com/products/8688)
* [Thermistor](https://www.sparkfun.com/products/250)

## Example Code To Use

* [Luminosity](/Examples/Sensors/Luminosity/Luminosity.ino)
* [Ambient_Light](/Examples/Sensors/Ambient_Light/Ambient_Light.ino)
* [Thermistor](/Examples/Sensors/Thermistor/Thermistor.ino)
* [Humidity](/Examples/Sensors/Humidity/Humidity.ino)
* [SD Card](/Examples/Concepts/SD_Simple/SD_Simple.ino)

## Hookup Guide

Make sure that the Arduino is connected to the breadboard as described in the [setup tutorial](/Worksheets/Setup.md).

In this project we are using multiple i2c sensors at the same time, which is unique. This will illustrate just how useful digital sensors are! All the i2c sensors communicate across the same two wires, SDA and SCL. Connect the Power and GND pins to 3.3v and GND respectivly, and the SDA/SCL Pins to pins 2 and 3 on the Arduino.

| Arduino | Sensors |
| ------: | :----- |
| Arduino Pin 2 -> | SDA |
| Arduino Pin 3 -> | SCL |
|  3.3v -> | 3v3 or + |
|  GND ->  | GND or - |

Connect the SD Card as described in the [SD Card Tutorial](/Worksheets/SD_Card.md).

Don't forget to connect the analog sensors! Reference the above image, and use any open Analog Input pins. [Here is a reference tutorial.](/Worksheets/Analog_Sensor.md)

Your setup MIGHT look something like this in the end. Note that the Temp/Humidity sensor is hiding the power connections, and the SD Card connections are not clear from the image.


![](/Worksheets/img/asra-25.jpg)



## Code

Use the examples listed above to test the connections of each sensor. Then combine them! See if you can combine code from the examples listed above to make this sensor work. If you run into trouble, the code is listed below.


```cpp
#include <SdFat.h>
#include <SparkFunTSL2561.h>
#include <SparkFun_Si7021_Breakout_Library.h>
#include <Thermistor.h>
#include <Wire.h>
#include <SPI.h>

SFE_TSL2561 luminosity;
Weather th;
Thermistor therm(A2); //It takes the Analog Pin Number as an argument.

boolean gain;     // Gain setting, 0 = X1, 1 = X16;
unsigned int ms;  // Integration ("shutter") time in milliseconds
double lux;    // Resulting lux value

float humidity = 0;
float tempf = 0;

int SD_CS = 10; //Chip Select pin for SD card

SdFat SD; //Declare the sd Card
File file; //this is the file that we are

int TEMPT_PIN = A1;
float temp;
int tempt;

long count = 0;

void setup() {
  Serial.begin(9600);
  luminosity.begin();
  gain = 0;
  unsigned char time = 1;
  luminosity.setTiming(gain, time, ms);
  luminosity.setPowerUp();
  th.begin();
  if (SD.begin(SD_CS)) { //Try initializing the Card, it will return a 1 if successful, and a 0 if not
    Serial.println("OK!");
  } else {
    Serial.println("FAILED!");
  }
}

void loop() {
  getLux();
  getTH();
  temp = therm.getTemp();
  tempt = analogRead(TEMPT_PIN);

  //Create a file name
  String fileName = String("HeatIsl.csv"); //Save is a CSV file
  char _fileName[fileName.length() + 1]; //convert String to Character Array
  fileName.toCharArray(_fileName, sizeof(_fileName));

  if (file.open(_fileName, FILE_WRITE)) { //Open the File
    //And write data to that file
//    Serial.print("writing data to ");
//    Serial.print(_fileName);
//    Serial.println();
    /*
       double lux;
       int tempt;
       float humidity;
       float tempf;
       float temp;
    */
    file.print(lux); file.print(",");
    file.print(tempt); file.print(",");
    file.print(humidity); file.print(",");
    file.print(tempf); file.print(",");
    file.print(temp); file.println();
    file.close();
    count++;
  } else {
    Serial.print("error opening: ");
    Serial.print(_fileName);
    Serial.println();
  }

  Serial.print(lux); Serial.print(",");
  Serial.print(tempt); Serial.print(",");
  Serial.print(humidity); Serial.print(",");
  Serial.print(tempf); Serial.print(",");
  Serial.print(temp); Serial.println();
  delay(10000);
}

void getLux()
{
  delay(ms);
  unsigned int data0, data1;
  if (luminosity.getData(data0, data1))
  {
    lux;    // Resulting lux value
    boolean good;  // True if neither sensor is saturated
    good = luminosity.getLux(gain, ms, data0, data1, lux);
  }
}

void getTH()
{
  humidity = th.getRH();
  tempf = th.getTempF();
}

float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

```
