# Setup

For this module we are using the Sparkfun Pro Micro development board for our projects. It is a little bit different from the most common Arduino, the Uno, but it plays nice with breadboards and is small and power efficient.

#### Board Diagram

![](https://cdn.sparkfun.com/assets/9/c/3/c/4/523a1765757b7f5c6e8b4567.png)

We always start with a blank breadboard.

![](/Worksheets/img/asra-118.jpg)

First step is to plug in the Arduino. Do it on the end of the breadboard with the USB port facing out.

![](/Worksheets/img/asra-117.jpg)

Next, connect it to the power busses to distribute power from the Arduino to the breadboard. The GND pin should be connected to the BLUE strip on the Power Bus of the breadboard on both sides. The VCC pin should be connected to the RED strip on one side of the breadboard. The two RED strips should be connected together with a long jumper wire.

![](/Worksheets/img/asra-116.jpg)

Now both power busses are supplied with a GND and 3.3 volt power from the Arduino board.

Next, connect a reset button. This is to reset the microcontroller if things crash or have problems. Pressing this button will momentarily connect the RST pin on the Arduino to GND, which restarts whatever code is on it.

![](/Worksheets/img/asra-115.jpg)

Now we can get to the fun stuff!
