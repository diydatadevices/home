# Blink

## Reading:
* [Variables (itp)](https://itp.nyu.edu/physcomp/lessons/programming/variables/)
* [Programing Terms (itp)](https://itp.nyu.edu/physcomp/lessons/programming/programming-terms-and-programming-environments/)

## Wiring
Connect pin 7 to a 200-500ohm resistor. Connect the resistor to the positive leg of an LED, connect the negetive end of the LED to GND. Remember that it matters what direction LED's go in. Current only flows one way. Connect the negetive side (flat edge/short leg) to GND.

![](/Worksheets/img/asra-2.jpg)

![](/Worksheets/img/asra-112.jpg)

## Code

```cpp
int led_pin = 7; //the Arduino pin we are sending information to

void setup() {
  pinMode(led_pin, OUTPUT); //set it as an output
}

void loop() {
  digitalWrite(led_pin, HIGH);
  delay(1000);
  digitalWrite(led_pin, LOW);
  delay(1000);
}
```

## Result

![](/Worksheets/img/LED.gif)
