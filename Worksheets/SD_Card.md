# SD Card

This is where your data is stored. It is important.

## Reading
* [SPI](https://learn.sparkfun.com/tutorials/serial-peripheral-interface-spi)
* And make sure you're familiar with the basic concepts of digital communication

## Hookup

![](/Worksheets/img/asra_sd-6.jpg)


| Arduino | SD Card | Function |
| ------: | :----- | :---: |
| Pin 15 | SCK or CLK | Clock |
| Pin 14 | DO | Digital Out |
| Pin 16 | DI | Digital In |
| Pin 10 | CS | Chip Select |
| N/A | CD | Card Detect |
| 3.3v | VCC | Power |
| GND | GND | Ground |

### Pin Explanations
* DO, Digital Out: Output line from SD Card, connects to MISO
* SCK, Clock: Clock signal from Master, connects to SCK
* DI, Digital In: Input line to SD Card, connects to MOSI
* CS, Chip Select: Selects the SD Card to be the active device on communication buss, connects to any digital output
* CD, Card Detect: Pin shorts to ground when SD Card is **not** inserted (requires pullup), connects to any digital input, we arn't using it in this example

## Code
The code for the SD Card can be complex. I'll try to break it down into simple pieces.

### Initilization
The SD Card must be initialized during the Setup, or it will not work. If the SD Card is removed and re-inserted without reseting the Arduino, no data can be written to the card. (this is why the Card Detect pin is very useful).

```cpp
//This is the library used to interact with SD Cards
#include "SdFat.h" //https://github.com/greiman/SdFat
int SD_CS = 10; //Chip Select pin for SD card
int CD_PIN = 8; //Card Detect pin

SdFat SD; //Declare the sd Card
File file; //this is the file that we are going to open and write data into

void setup() {
  Serial.begin(9600);
  while(!Serial); //Wait for Serial Port to open before running
  Serial.println("--SD Card Write Example--");
  pinMode(CD_PIN, INPUT_PULLUP); //set Card Detect to input with internal Pullup engaged
  Serial.print("SD initializing. . . ");
  if(SD.begin(SD_CS)) { //Try initializing the Card, it will return a 1 if successful, and a 0 if not
    Serial.println("OK!");
  } else {
    Serial.println("FAILED!");
  }
}
```

Note that this code assumes that the card is inserted. If it isn't, it will fail but won't give a reason for it.

### Loop

During the Loop portion of the code, generate two pieces of data and store them on the SD Card as a CSV file every 5 seconds.

```cpp
void loop() {
  //Create Variables for Dummy Data
  int dummy_int;
  float dummy_float;
  //Fill those variables
  dummy_int = random(0, 1023); //creates a random number between 0 and 1023
  dummy_float = float(random(0, 10)) * PI; //creates a dummy floating point dumber

  //Create a file name
  String fileName = String("random.csv"); //Save is a CSV file
  char _fileName[fileName.length() + 1]; //convert String to Character Array
  fileName.toCharArray(_fileName, sizeof(_fileName));

  if (file.open(_fileName, FILE_WRITE)) { //Open the File
    //And write data to that file
    Serial.print("writing data to ");
    Serial.print(_fileName);
    Serial.println();
    file.print(dummy_int); file.print(",");
    file.print(dummy_float); file.println();
    file.close();
  } else {
    Serial.print("error opening: ");
    Serial.print(_fileName);
    Serial.println();
  }
  delay(5000);
}
```

Then, remove the SD Card from the shield and plug it into the computer and see if the file exists! And see if there is data on it!

#### How can you change how often the data is being recorded?
#### What if you want to only record data once every hour?
#### How would you integrate sensor readings into this code?
