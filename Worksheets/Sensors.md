# List of Devices

## Sensors and Documentation

* [Thermistor](https://www.sparkfun.com/products/250)
  * [Datasheet](http://cdn.sparkfun.com/datasheets/Sensors/Temp/ntcle100.pdf)
  * Analog sensor that measures temperature. It is a resistor that changes resistance with temperature. All resistors are affected by temperature to some degree, but this usually isn't desireable.
  * Example: To find out how buildings affect the environment, measure air temperature near windows and concrete.

* [Humidity](https://www.sparkfun.com/products/13763)
  * [Datasheet](https://cdn.sparkfun.com/datasheets/Sensors/Weather/Si7021.pdf)
  * Digital sensor that measures [Relative Humidity](https://en.wikipedia.org/wiki/Relative_humidity)(RH), and Temperature. RH is the relationship of water vapor in the air to temperature and pressure of the air.
  * Example: If you measure RH, temperature, and light intensity in the forest, you can calculate how much water trees are transpiring or 'breathing out'.

* [Barometric Pressure](https://www.sparkfun.com/products/13848)
  * [Datasheet](https://github.com/sparkfun/BMP180_Breakout/raw/master/Documentation/BMP180%20Datasheet%20V2.5.pdf)
  * Digital sensor that measures atomspheric pressure. Atmospheric pressure is used to predict weather and measure altitude.
  * Example: The higher you are above sea level, the lower the pressure. Likewise, weather systems change barometric pressure. Large storms tend to cause pressure drops, and high pressure zones are known for clear skys and fair weather.

* [Soil Moisture](https://www.sparkfun.com/products/13322)
  * [Hookup Guide](https://learn.sparkfun.com/tutorials/soil-moisture-sensor-hookup-guide)
  * Analog sensor which measures moisture in the soil. It is analog, so it will need calibration before it will give meaningful quantitative measurements. There are also a few different types of [soil moisture](https://en.wikipedia.org/wiki/Soil_moisture_sensor), so it's worth reading.
  * Example: Obviously, it's for making your house plants [text you when they need to be watered](http://www.instructables.com/id/Internet-of-Dirt-a-Texting-Plant/).

* [TEMT6000 - Ambient Light Sensor](https://www.sparkfun.com/products/8688)
  * [Datasheet](https://cdn.sparkfun.com/assets/learn_tutorials/2/5/9/temt6000.pdf).
  * Analog sensor that measures light intensity. It is analog, so it will need to be calibrated before it will give meaningful measurements.
  * Example: One really cool device that uses a simple light sensor and a clock is [this geolocator for migrating birds](https://cdn.sparkfun.com/assets/learn_tutorials/2/5/9/temt6000.pdf). Light sensors are used as really cheap proximity detectors too, to detect shadows and trigger an action, like [this terrible singing fish](https://www.youtube.com/watch?v=m80jt6bm6ZA).

* [UV](https://www.sparkfun.com/products/12705)
  * [Datasheet](https://cdn.sparkfun.com/datasheets/Sensors/LightImaging/ML8511_3-8-13.pdf)
  * Analog sensor that is sensitive to the UV spectrum of light (300-400nm). Its output is linearly related to the UV intensity, so calibration is a simple equation. We can't see UV light, but it is what causes sun burns and snow-blindness.
  * Neat fact: [Reindeer can see the UV spectrum](http://www.bbc.com/news/science-environment-13529152), why?

* [RGB](https://www.sparkfun.com/products/12829)
  * [Datasheet](https://cdn.sparkfun.com/tutorialimages/ISL29125/isl29125.pdf)
  * Digital sensor that measures light color. It quantifies RED, GREEN, and BLUE values of whatever it is pointed at. Color is a complex subject and has a lot to do with our own perception.
  * Example: [Look at this Kickstarter product](https://www.kickstarter.com/projects/nixsensor/nix-color-sensor/description) that likely uses this exact sensor to collect colors from the real world and to use digitally.

* [Luminosity](https://www.sparkfun.com/products/12055)
  * [Datasheet](http://cdn.sparkfun.com/datasheets/Sensors/LightImaging/TSL2561.pdf)
  * This is a digital sensor that measures light intensity. [This video](https://www.sparkfun.com/videos#all/FYCiO6hRRt8/51) describes what luminosity is. Light is complex, but we can think of this sensor as a light sensor weighted to human perception of brightness. It outputs in units of Lux. [The wikipedia article on Illuminance is quite good](https://en.wikipedia.org/wiki/Illuminance), "'Brightness' should never be used for quantitative description, but only for nonquantitative references to physiological sensations and perceptions of light."
  * Example: Which lights in your household give off the most light?

* [Motion](https://learn.sparkfun.com/tutorials/openpir-hookup-guide)
  * [Datasheet](https://cdn.sparkfun.com/datasheets/Sensors/Proximity/NCS36000-D.PDF)
  * This is a sensor which outputs a boolean (true or false) value depending on motion of a warm blooded animal (such as a human). It uses a single pixel Infrared detector to detect the presence of a warm body. That doesn't sound creepy.....
  * Example: These sensors are in most buildings to automatically turn lights on and off depending on occupancy. Can you find one in the room you're in now? They're also used in camera traps for monitoring wildlife, or [for keeping your cat off the kitchen counter](https://www.youtube.com/watch?v=-b9m8BpmD0o).

* [Sound](https://learn.sparkfun.com/tutorials/sound-detector-hookup-guide)
  * [Datasheet](http://cdn.sparkfun.com/datasheets/Sensors/Sound/LMV324.pdf)
  * Analog sensor that measures amplitude (loudness) and the presence or absence sound. It has a trigger output that flips on when a sound over a certain loudness threshold occurs. It also has a amplitude output, which is an analog value that maps to the amplitude of the audio input. It's also known as an [envelope follower](https://en.wikipedia.org/wiki/Envelope_detector) and could be calibrated to the loudness of a sound.
  * Example: Using the trigger output one could build a device to measure how often loud things happen, for example, [the initiative in Denali National Park that is starting to monitor how much noise pollution there is from aircraft flying within the park.](https://www.nature.nps.gov/ParkScience/index.cfm?ArticleID=350)

* [Pulse](https://www.sparkfun.com/products/11574)
  * [Tutorial Video](https://www.youtube.com/watch?v=82T_zBZQkOE&feature=youtu.be)
  * Analog sensor that measures how fast your heart beats.
  * Example: Do older or younger people have faster heartbeats? You could measure adults and kids with this sensor to find out.

* [Force sensitive resistor](https://www.sparkfun.com/products/9375)
  * [Datasheet](http://www.sparkfun.com/datasheets/Sensors/Pressure/fsrguide.pdf)
* [Flexiforce](https://www.sparkfun.com/products/11207)
  * [Datasheet](http://cdn.sparkfun.com/datasheets/Sensors/Pressure/A401-force-sensor.pdf)
  * Both of these are analog sensors that measure pressure. They are not calibrated and differ slightly in their pressure limits and response curves.
  * Example: They could be used for measuring relative weights, for example, monitoring the weight of a potted plant to see when it needs to be watered.

* [Piezo Vibration](https://www.sparkfun.com/products/9198)
  * [Datasheet](http://www.sparkfun.com/datasheets/Sensors/Flex/MiniSense_100.pdf)
  * Analog sensors which are somewhat unique and versatile. They are typically used to measure vibration or presence.
  * Example: You could detect when a bird entered its nest by attaching a piece of grass to the sensor so that it would be disturbed when the bird entered or left the nest. They could be used to act as triggers for other events, or monitor how often squirrels run across your porch.

* [Methane](https://www.sparkfun.com/products/9404)
  * [Datasheet](https://cdn.sparkfun.com/datasheets/Sensors/Biometric/MQ-4%20Ver1.3%20-%20Manual.pdf)
  * Analog sensor that measures methane gas concentration in the air. [Methane](https://en.wikipedia.org/wiki/Methane) is produced by anarobic decomposition and is a potent greenhouse gas.
  * Example: [This is a project which tracks how often you fart using this same sensor](https://www.kickstarter.com/projects/963861855/keep-track-of-your-gases-with-ch4).

* [Indoor Air quality - CCS811](https://www.sparkfun.com/products/14193)
  * [Datasheet](https://cdn.sparkfun.com/assets/learn_tutorials/1/4/3/CCS811_Datasheet-DS000459.pdf)
  * This is a sensor that measures Total Volitile Organic Compounds, or TVOCs, and equivalent Carbon Dioxide, or eCO2. This sensor is designed for monitoring indoor air quality and measuring how many people are in a room to control ventilation systems. [This is a good video introducing it and explaining a little bit about TVOCs](https://youtu.be/gdFb6-4bsHk).
  * Example: This would be useful for measuring occupancy of rooms or investigating how ventilation systems work.

* [Dust](https://www.sparkfun.com/products/9689)
  * [Datasheet](http://www.sparkfun.com/datasheets/Sensors/gp2y1010au_e.pdf)
  * Analog sensor that measures fine particulates in the air such as dust and smoke. It is commonly used in air purification systems. Here is an excellent [paper on comparing these low cost dust/particulate sensors to expensive professional devices](http://www.tandfonline.com/doi/pdf/10.1080/02786826.2015.1100710?needAccess=true).
  * Example: This sensor would be very useful for monitoring and reporting winter air pollution in the Fairbanks area. It could be paired with a system similar to the texting house-plant to generate a warning on bad air days.


  ## MCU
  * [Pro Micro](https://www.sparkfun.com/products/12587)
