/**
 *
 * Hardware Connections:
 * Sensor ------------- Arduino
 * Black -------------- GND
 * Red ---------------- 3.3V (VCC)
 * Purple ------------- A0
 *
 * This shows one way to interact with the pulse sensor.
 * This code calculates the time delay between
 * voltage spikes (active pulses) from the
 * sensor and prints out an average Beats Per Minute
 * for ever 3 pulses that happen.
 *
*/

int Sensor_Pin = A0; //pin that the sensor is attached to
int value; //variable to store the value
long current_ms; //current time
long last_ms = 0; //last pulse time
boolean pulse = false; //pulse status

int Threshold = 600;
int pulse_count = 10; //number of pulses that have happened
int pulse_num = 0; //number of pulses to average
int total_time = 0; //time to average

void setup() {
  Serial.begin(9600); //begin Serial Communications
}

void loop() {
  current_ms = millis();
  value = analogRead(Sensor_Pin); //Read value from Analog to Digital Converter
  if (value > Threshold && pulse == false) { //the analog value goes up as a pulse happens. Toy with this value if it isn't working right
    pulse = true; //a pulse is happening

    Serial.print("."); //print in indicator that a pulse happened
    if (pulse_num < pulse_count-1) { //it's -1 because it's zero indexed
      total_time = total_time + (current_ms - last_ms); //add all times together to create average
      pulse_num++; //increment pulse number
      last_ms = current_ms; //save the current time for next pulse
    } else {
      Serial.print(60000/(total_time/pulse_count)); //calculate BPM
      Serial.println(" bpm");
       //reset averaging values
      pulse_num = 0;
      total_time = 0;
    }
    delay(100);
  } else {
    pulse = false; //reset pulse status
  }
}
