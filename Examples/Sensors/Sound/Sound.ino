/**
 *
 * Hardware Connections:
 * Sensor --------------- Arduino
 * (GND) ---------------- GND
 * (3V3) ---------------- 3.3V (VCC)
 * GATE ----------------- D4
 * ENVELOPE ------------- A0
 * AUDIO ---------------- DO NOT CONNECT
 *
 * This sensor detects sound and can measure loudness.
 * It is an analog sensor, so it will need calibration
 * to get meaningful values out of it.
 *
 * The GATE pin is a digital output which is pulled HIGH
 * when there is a sound louder than the Threshold.
 * The ENVELOPE pin is an Analog Ouput, which is a value
 * matched to the loudness of the sound that was detected.
 *
 * This sketch takes measurements from the gate and the envelope pins
 * and outputs the loudest sounds from each trigger open event.
 *
 */


int gate_pin = 4; //gate pin
int env_pin = A0; //envelope pin
int loudness; //the analog input reading
int loudest; //the loudest sound
boolean trig; //gate status

long ms_start; //the time that the trigger opened

void setup() {
  Serial.begin(9600); //start serial
  pinMode(gate_pin, INPUT); //the gate input is an input
}

void loop() {
  trig = digitalRead(gate_pin); //read the gate pin
  if (trig == true) { //if the gate opens, it will return a TRUE
    loudness = analogRead(env_pin); //read the envelope
    if (loudness > loudest) { //if the current sound is louder than the loudest one...
      loudest = loudness; //save the current one as the loudest one
      //print out values
      // Serial.print(millis() - ms_start); //number of milliseconds since the gate opened
      // Serial.print(" ");
      // Serial.println(loudness);
    }
  } else {
    if(loudest != 0) Serial.println(loudest);
    //this is running while the gate is closed
    ms_start = millis(); //save the current time
    loudest = 0; //reset the loudest sound to 0 after each gate event
  }
}
