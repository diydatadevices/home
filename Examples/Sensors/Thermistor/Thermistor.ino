/**
 * This example uses a Thermistor Library to caluculate
 * temperature from an analog sensor.
 */

#include <Thermistor.h>

Thermistor temp(A0); //Thermistor is connected to Analog pin 0

float temperature;

void setup() {
  Serial.begin(9600); //initialize Serial Connection
}

void loop() {
  temperature = temp.getTemp(); //returns the Temperature value of the thermistor
  //prints out temperature values
  Serial.print("Temperature: ");
  Serial.print(temperature);
  Serial.println("deg C");
  delay(1000); //delay 1 second
}
