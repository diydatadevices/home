/**
 *
 * Hardware Connections:
 * Sensor ------------- Arduino
 * (GND) -------------- GND
 * VCC ---------------- 3.3V (VCC)
 * OUT ---------------- D4
 * A ------------------ DO NOT CONNECT
 *
 * Example for the OpenPIR Sensor
 * The sensor needs to be powered on for about 2 minutes before it becomes active.
 * Once active it will respond to motion.
 * If triggered, the OUT pin will go HIGH for about 5 seconds before returning to its LOW state.
 */

int motion_pin = 4;
boolean motion_state = 0;

void setup() {
  Serial.begin(9600);
  pinMode(motion_pin, INPUT); //set it as an output
}

void loop() {
  motion_state = digitalRead(motion_pin);
  if(motion_state == HIGH) {
    Serial.println("MOTION HAS BEEN DETECTED!");
    delay(1000);
  }
}
