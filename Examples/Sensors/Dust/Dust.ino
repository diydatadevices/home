/**
 * Hardware Connections:
 * Sensor -------------- Arduino
 * WHITE --------------- conn to Resistor to VCC, and Capacitor to GND
 * BLUE ---------------- GND
 * GREEN --------------- D4
 * YELLOW -------------- GND
 * BLACK --------------- A0
 * RED ----------------- VCC
 *
 * Tutorial/bad wiring diagrams: http://arduinodev.woofex.net/2012/12/01/standalone-sharp-dust-sensor/
 * More Documentation: https://gitlab.com/diydatadevices/home/blob/master/Worksheets/GP2Y1010AU0F.md
 *
 * Dust/Particulate sensor.
 * This sensor has an Analog Output. Look at the documentation.
 *
 */

 int dustPin = 0; // dust sensor - Arduino A0 pin
 int ledP = 4;

 float voMeasured = 0;
 float calcVoltage = 0;
 float dustDensity = 0;

 void setup(){
   Serial.begin(9600);
   pinMode(ledP,OUTPUT);
 }

 void loop(){
   digitalWrite(ledP,LOW); // power on the LED
   delayMicroseconds(280);

   voMeasured = analogRead(dustPin); // read the dust value

   delayMicroseconds(40);
   digitalWrite(ledP,HIGH); // turn the LED off
   delayMicroseconds(9680);

   // 0 - 3.3V mapped to 0 - 1023 integer values
   // recover voltage
   calcVoltage = voMeasured * (3.3 / 1024.0);
   dustDensity = 0.17 * calcVoltage - 0.1;

   Serial.print("Raw Signal Value(0-1023) : ");
   Serial.print(voMeasured);

   Serial.print(" - Voltage: ");
   Serial.print(calcVoltage);

   Serial.print(" - Dust Density: ");
   Serial.println(dustDensity); // mg/m3

   delay(1000);
 }
