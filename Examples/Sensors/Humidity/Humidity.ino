/**
 * Hardware Connections:
 * Sensor --------------- Arduino
 * (-) ------------------ GND
 * (+) ------------------ 3.3V (VCC)
 * CL ------------------- D3/SCL
 * DA ------------------- D2/SDA
 */


#include "SparkFun_Si7021_Breakout_Library.h"
#include <Wire.h>

float humidity = 0;
float tempf = 0;


Weather th; //Create Instance of SI7021 temp and humidity sensor


void setup(){
    Serial.begin(9600);   // open serial over USB at 9600 baud
    th.begin(); //Initialize the I2C sensor and ping them

}

void loop(){

  humidity = sensor.getRH(); // Measure Relative Humidity from the Si7021


  tempf = sensor.getTempF(); // Measure Temperature from the Si7021
  // Temperature is measured every time RH is requested.
  // It is faster, therefore, to read it from previous RH
  // measurement with getTemp() instead with readTemp()

    Serial.print("Temp:");
    Serial.print(tempf);
    Serial.print("F, ");

    Serial.print("Humidity:");
    Serial.print(humidity);
    Serial.println("%");

    delay(1000);
}
