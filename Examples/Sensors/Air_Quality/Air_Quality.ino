/**
 *
 * Hardware Connections:
 * Sensor --------------- Arduino
 * (GND) ---------------- GND
 * (3V3) ---------------- 3.3V (VCC)
 * SDA ------------------ D2
 * SCL ------------------ D3
 * WAK ---------------- DO NOT CONNECT
 * INT ---------------- DO NOT CONNECT
 * RST ---------------- DO NOT CONNECT
 *
 *
 * Read the TVOC and CO2 values from the SparkFun CSS811 breakout board
 * This is the simplest example.  It throws away most error information and
 * runs at the default 1 sample per second.
 *
 * A new sensor requires at 48-burn in. Once burned in a sensor requires
 * 20 minutes of run in before readings are considered good.
 */

#include "SparkFunCCS811.h"

#define CCS811_ADDR 0x5B //Default I2C Address

CCS811 sensor(CCS811_ADDR);

void setup()
{
  Serial.begin(9600);
  Serial.println("CCS811 Basic Example");

  //It is recommended to check return status on .begin(), but it is not
  //required.
  CCS811Core::status returnCode = sensor.begin();
  if (returnCode != CCS811Core::SENSOR_SUCCESS)
  {
    Serial.println(".begin() returned with an error.");
    while (1); //Hang if there was a problem.
  }
}

void loop()
{
  //Check to see if data is ready with .dataAvailable()
  if (sensor.dataAvailable()) {
    //If so, have the sensor read and calculate the results. We'll get them later
    sensor.readAlgorithmResults();

    Serial.print("eCO2[");
    //Returns calculated eCO2 reading
    Serial.print(sensor.getCO2());
    Serial.print("] sensor[");
    //Returns calculated TVOC reading
    Serial.print(sensor.getTVOC());
    Serial.println();
  }
  delay(100);
}
