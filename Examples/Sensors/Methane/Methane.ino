/**
 *
 * Hardware Connections:
 * Sensor ------------- Arduino
 * Black -------------- GND
 * Red ---------------- 3.3V (VCC)
 * Brown -------------- A0
 *
 * Try this. We might need to tweak the sensor a little bit.
 * Try testing it with a butane lighter that is not lit, see
 * how the values change.
 *
 */

int sensorPin = A0;
int sensorVal;

void setup() {
  Serial.println(9600);
}

void loop() {
  sensorVal = analogRead(sensorPin);
  Serial.println(sensorVal);
  delay(100);
}
