/**
 *
 * Hardware Connections:
 * Sensor --------------- Arduino
 * (GND) ---------------- GND
 * (3V3) ---------------- 3.3V (VCC)
 * SCL ------------------ D3
 * SDA ------------------ D2
 * VDDIO ---------------- DO NOT CONNECT
 *
 * This sketch shows how to use the SFE_BMP180 library to read the
 * Bosch BMP180 barometric pressure sensor.
 * https://www.sparkfun.com/products/11824
 *
 * Like most pressure sensors, the BMP180 measures absolute pressure.
 * This is the actual ambient pressure seen by the device, which will
 * vary with both altitude and weather.
 *
 * Before taking a pressure reading you must take a temparture reading.
 * This is done with startTemperature() and getTemperature().
 * The result is in degrees C.
 *
 * Once you have a temperature reading, you can take a pressure reading.
 * This is done with startPressure() and getPressure().
 * The result is in millibar (mb) aka hectopascals (hPa).
 *
 * If you'll be monitoring weather patterns, you will probably want to
 * remove the effects of altitude. This will produce readings that can
 * be compared to the published pressure readings from other locations.
 * To do this, use the sealevel() function. You will need to provide
 * the known altitude at which the pressure was measured.
 */

#include <SFE_BMP180.h> //Sensor Library
#include <Wire.h> //i2c library

SFE_BMP180 pressure; //Creates the sensor

float ALTITUDE = 182.88; //Alitude of UAF in Meters (check this)
char status; //varibale to hold device status
double T; //Temperature
double P; //Pressure
double p0; //Compensated pressure at 0 altitude

void setup()
{
  Serial.begin(9600);
  if (pressure.begin()){ // Initialize the sensor (it is important to get calibration values stored on the device).
    Serial.println("BMP180 init success");
  } else {
    Serial.println("BMP180 init fail\n\n");// Oops, something went wrong, this is usually a connection problem,
    while(1); // Pause forever.
  }
}

void loop()
{
  // You must first get a temperature measurement to perform a pressure reading.
  // Start a temperature measurement:
  // If request is successful, the number of ms to wait is returned.
  // If request is unsuccessful, 0 is returned.
  status = pressure.startTemperature();
  if (status != 0) {
    delay(status); // Wait for the measurement to complete:
    status = pressure.getTemperature(T); // Retrieve the completed temperature measurement, Stored in T.
    if (status != 0) {
      Serial.print("temperature: ");
      Serial.print(T,2);
      Serial.print(" deg C, ");
      status = pressure.startPressure(3); // Start a pressure measurement: The parameter is the oversampling setting, from 0 to 3 (highest res, longest wait).
      if (status != 0) {
        delay(status); // Wait for the measurement to complete:
        status = pressure.getPressure(P,T); // Retrieve the completed pressure measurement: Note that the measurement is stored in the variable P.
        if (status != 0){
          // Print out the measurement:
          Serial.print("absolute pressure: ");
          Serial.print(P,2);
          Serial.print(" mb, ");

          // The pressure sensor returns abolute pressure, which varies with altitude.
          // To remove the effects of altitude, use the sealevel function and your current altitude.
          // Parameters: P = absolute pressure in mb, ALTITUDE = current altitude in m.
          // Result: p0 = sea-level compensated pressure in mb

          p0 = pressure.sealevel(P,ALTITUDE);
          Serial.print("relative (sea-level) pressure: ");
          Serial.print(p0,2);
          Serial.print(" mb, ");
        }
        else Serial.println("error retrieving pressure measurement\n");
      }
      else Serial.println("error starting pressure measurement\n");
    }
    else Serial.println("error retrieving temperature measurement\n");
  }
  else Serial.println("error starting temperature measurement\n");

  delay(5000);  // Pause for 5 seconds.
}
