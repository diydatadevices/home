/**
 * This is the basic code to read an Analog Value
 * using the built in Analog to Digital Converter
 * on the Arduino.
 * The value is printed to the Serial Monitor
 * once per 1000 milliseconds.
 */

int Sensor_Pin = A0; //pin that the sensor is attached to
int value; //variable to store the value

void setup() {
  Serial.begin(9600); //begin Serial Communications
}

void loop() {
  value = analogRead(Sensor_Pin); //Read value from Analog to Digital Converter
  Serial.println(value); //Print value to Serial Monitor
  delay(1000); //Wait for 1 second
}
