/**
 * Hardware Connections:
 * Sensor --------------- Arduino
 * (GND) ---------------- GND
 * (VCC) ---------------- 3.3V (VCC)
 * SCL ------------------ D3
 * SDA ------------------ D2
 * INT ------------------ DO NOT CONNECT
 *
 * This sensor measures Red, Green, and Blue light intensity, thereby measuring color!
 */

#include <Wire.h>
#include "SFE_ISL29125.h"

SFE_ISL29125 RGB_sensor;

void setup()
{
  Serial.begin(9600); // Initialize serial communication
  if (RGB_sensor.init()) { // Initialize the ISL29125 with simple configuration so it starts sampling
    Serial.println("Sensor Initialization Successful");
  }
}


void loop()
{
  // Read sensor values (16 bit integers)
  unsigned int red = RGB_sensor.readRed();
  unsigned int green = RGB_sensor.readGreen();
  unsigned int blue = RGB_sensor.readBlue();

  // Print out readings
  Serial.print("Red: ");
  Serial.println(red);
  Serial.print("Green: ");
  Serial.println(green);
  Serial.print("Blue: ");
  Serial.println(blue);
  Serial.println();
  delay(1000);
}
