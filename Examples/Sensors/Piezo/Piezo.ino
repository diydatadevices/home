/**
 *
 * Hardware Connections:
 * Sensor --------------- Arduino
 * minus ---------------- GND
 * plus ----------------- A0
 *
 * This code uses the Piezo sensor to detect values over a threshold
 */


int sensorPin = A0;
int sensorVal;
int threshold = 700;

boolean trig = false;

void setup() {
  Serial.println(9600);
}

void loop() {
  sensorVal = analogRead(sensorPin);
  if(sensorVal > threshold && trig == false) {
    trig = true;
    Serial.println("KNOCK!");
    delay(100); //wait for the values to return to normal
  } else {
    trig = false; //reset trigger value
  }
}
