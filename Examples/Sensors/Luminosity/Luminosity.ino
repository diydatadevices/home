/**
 * Hardware Connections:
 * Sensor --------------- Arduino
 * (GND) ---------------- GND
 * (VCC) ---------------- 3.3V (VCC)
 * SCL ------------------ D3
 * SDA ------------------ D2
 * INT ------------------ DO NOT CONNECT
 *
 * Product page: https://www.sparkfun.com/products/11824
 * Hook-up guide: https://learn.sparkfun.com/tutorials/getting-started-with-the-tsl2561-luminosity-sensor
 * // If time = 0, integration will be 13.7ms
 // If time = 1, integration will be 101ms
 // If time = 2, integration will be 402ms
 // If time = 3, use manual start / stop to perform your own integration
*/

#include <SparkFunTSL2561.h>
#include <Wire.h>

SFE_TSL2561 light; // Create an SFE_TSL2561 object, here called "light":

boolean gain = 0;     // Gain setting, 0 = X1, 1 = X16;
int time = 1; //the amount of time the "Shutter" will be open
/**
 * time = 0, shutter time is 13.7ms
 * time = 1, shutter time is 101ms <-- This is best for full sunlight
 * time = 2, shutter time is 402ms
 * time = 3, shutter time is manual
 */
long ms;  // Integration ("shutter") time in milliseconds
double lux;    // Resulting lux value
boolean good;  // True if neither sensor is saturated


void setup()
{
  Serial.begin(9600);   // Initialize the Serial port:
  light.begin(); // Initialize the SFE_TSL2561 library
  light.setTiming(gain,time,ms); //Sets the mode/options of sensor
  light.setPowerUp(); //power on the sensor

  // The sensor will now gather light during the integration time.
  // After the specified time, you can retrieve the result from the sensor.
  // Once a measurement occurs, another integration period will start.
}

void loop()
{

  delay(ms); // Wait between measurements before retrieving the result
  // There are two light sensors on the device, one for visible light
  // and one for infrared. Both sensors are needed for lux calculations.
  unsigned int data0, data1;
  if (light.getData(data0,data1)){   // Retrieve the data from the device:
    good = light.getLux(gain,ms,data0,data1,lux); // Perform lux calculation:
    // The getLux() function will return 1 if the calculation
    // was successful, or 0 if one or both of the sensors was
    // saturated (too much light).
    Serial.print("data0: ");
    Serial.print(data0);
    Serial.print(" data1: ");
    Serial.println(data1);

    if (good) {
      Serial.print("Lux: ");
      Serial.println(lux);
    } else {
      Serial.println("(BAD)");
    }
  }
  else {
    byte error = light.getError(); // getData() returned false because of an I2C error, inform the user.
    printError(error);
  }
}

void printError(byte error)
  // If there's an I2C error, this function will
  // print out an explanation.
{
  Serial.print("I2C error: ");
  Serial.print(error,DEC);
  Serial.print(", ");

  switch(error)
  {
    case 0:
      Serial.println("success");
      break;
    case 1:
      Serial.println("data too long for transmit buffer");
      break;
    case 2:
      Serial.println("received NACK on address (disconnected?)");
      break;
    case 3:
      Serial.println("received NACK on data");
      break;
    case 4:
      Serial.println("other error");
      break;
    default:
      Serial.println("unknown error");
  }
}
