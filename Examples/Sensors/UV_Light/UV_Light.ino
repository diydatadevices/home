/**
 * look up equations for mapping analog value to mW/cm^2
 * Check datasheet for sensor
 */

int UVOUT = A0; //Output from the sensor
int uvLevel;
float uvIntensity; //mW/cm^2

void setup() {
  Serial.begin(9600); //begin Serial Communications
}

void loop() {
  uvLevel = analogRead(UVOUT); //Read Analog Input
  // uvIntensity = mapfloat(uvLevel, 0.99, 2.9, 0.0, 15.0); //Map input to mW/cm^2
  uvIntensity = (uvLevel - 0.99) * 15.0/1.91;
  Serial.println(value); //Print value to Serial Monitor
  delay(1000); //Wait for 1 second
}

float mapfloat(float x, float in_min, float in_max, float out_min, float out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
