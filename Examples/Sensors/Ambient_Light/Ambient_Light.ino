// https://www.sparkfun.com/products/8688
// http://bildr.org/2011/06/temt6000_arduino/

/**
 * This is a basic Analog Sensor.
 * Values will be between 0 and 1023, but will need
 * calibration against a calibrated sensor to get
 * meaningful outputs. Check datasheet for device to
 * learning WHAT the light wavelengths the sensor actually
 * measures.
**/

int Sensor_Pin = A0;
int value;
void setup() {
  Serial.begin(9600);
}

void loop() {
  value = analogRead(Sensor_Pin);
  Serial.println(value);
  delay(1000);
}
