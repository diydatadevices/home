/**
 * Pin Hookup Guide: (for adafruit shield)
 * :Arduino:   :SD Card:
 *    15    -->   CLK
 *    14    -->   DO
 *    16    -->   DI
 *    10    -->   CS
 *    GND    -->   GND
 *    VCC    -->   3v
 **/


#include "SdFat.h" //https://github.com/greiman/SdFat

int LED = 8; //LED pin
int SD_CS = 10; //Chip Select pin for SD card
int count = 0;
int CD_PIN = 8;

SdFat SD; //Declare the sd Card

void setup() {
  Serial.begin(9600);
  Serial.println("--SD Card Write Example--");
  bool SD_OK = false; //Variable to check that the SD Card has Initialized
  pinMode(LED, OUTPUT);
  digitalWrite(LED, HIGH); //turn LED on to signal start of test
  if (SD.begin(SD_CS)) { //Try initializing the Card...
    Serial.print("SD initialize...");
    File f; //declare a File
    if (f.open("init.txt", FILE_WRITE)) { //Try opening that File
      Serial.println("OK!");
      //Print to open File
      f.print("This is a random number: ");
      f.print(random(0, 1000));
      f.println();
      f.close(); //Close file
      SD_OK = true; //SD card is OK!
      digitalWrite(LED, LOW); //turn LED off
    }
  }
  if (!SD_OK) { //If SD is not ok, blink forever.
    while (1) {
      digitalWrite(LED, HIGH);
      delay(200);
      digitalWrite(LED, LOW);
      delay(200);
    }
  }
}

void loop() {
  float data5 = float(random(0, 10)) * PI;
  //Create Dummy Data
  int data1 = random(0, 1023);
  int data2 = random(0, 255);
  int data3 = random(0, 1);
  File f; //declares a File
   String fileName = String("rondom.csv"); //Save is a CSV file...because
  char _fileName[fileName.length() + 1]; //convert String to Char Array
  fileName.toCharArray(_fileName, sizeof(_fileName));

  digitalWrite(LED, HIGH); //set LED high to signal SD Write
  if (f.open(_fileName, FILE_WRITE)) { //Open the File
    //And write data to that file
    Serial.print("writing data to ");
    Serial.print(_fileName);
    Serial.println();
    f.print(count); f.print(",");
    f.print(data1); f.print(",");
    f.print(data2); f.print(",");
    f.print(data3); f.println();
    f.close();
    //    digitalWrite(LED, LOW);
    count++;
  } else {
    Serial.print("error opening: ");
    Serial.print(_fileName);
    Serial.println();
  }
  delay(2000);
}
