#include <SdFat.h>
#include <SparkFunTSL2561.h>
#include <SparkFun_Si7021_Breakout_Library.h>
#include <Thermistor.h>
#include <Wire.h>
#include <SPI.h>

SFE_TSL2561 luminosity;
Weather th;
Thermistor therm(A2); //It takes the Analog Pin Number as an argument.

boolean gain;     // Gain setting, 0 = X1, 1 = X16;
unsigned int ms;  // Integration ("shutter") time in milliseconds
double lux;    // Resulting lux value

float humidity = 0;
float tempf = 0;

int SD_CS = 10; //Chip Select pin for SD card

SdFat SD; //Declare the sd Card
File file; //this is the file that we are

int TEMPT_PIN = A1;
float temp;
int tempt;

long count = 0;

void setup() {
  Serial.begin(9600);
  luminosity.begin();
  gain = 0;
  unsigned char time = 1;
  luminosity.setTiming(gain, time, ms);
  luminosity.setPowerUp();
  th.begin();
  if (SD.begin(SD_CS)) { //Try initializing the Card, it will return a 1 if successful, and a 0 if not
    Serial.println("OK!");
  } else {
    Serial.println("FAILED!");
  }
}

void loop() {
  getLux();
  getTH();
  temp = therm.getTemp();
  tempt = analogRead(TEMPT_PIN);

  //Create a file name
  String fileName = String("HeatIsl.csv"); //Save is a CSV file
  char _fileName[fileName.length() + 1]; //convert String to Character Array
  fileName.toCharArray(_fileName, sizeof(_fileName));

  if (file.open(_fileName, FILE_WRITE)) { //Open the File
    //And write data to that file
//    Serial.print("writing data to ");
//    Serial.print(_fileName);
//    Serial.println();
    /*
       double lux;
       int tempt;
       float humidity;
       float tempf;
       float temp;
    */
    file.print(lux); file.print(",");
    file.print(tempt); file.print(",");
    file.print(humidity); file.print(",");
    file.print(tempf); file.print(",");
    file.print(temp); file.println();
    file.close();
    count++;
  } else {
    Serial.print("error opening: ");
    Serial.print(_fileName);
    Serial.println();
  }

  Serial.print(lux); Serial.print(",");
  Serial.print(tempt); Serial.print(",");
  Serial.print(humidity); Serial.print(",");
  Serial.print(tempf); Serial.print(",");
  Serial.print(temp); Serial.println();
  delay(10000);
}

void getLux()
{
  delay(ms);
  unsigned int data0, data1;
  if (luminosity.getData(data0, data1))
  {
    lux;    // Resulting lux value
    boolean good;  // True if neither sensor is saturated
    good = luminosity.getLux(gain, ms, data0, data1, lux);
  }
}

void getTH()
{
  humidity = th.getRH();
  tempf = th.getTempF();
}

float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
