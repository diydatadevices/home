# Analog vs. Digital

## Reading:

* [Analog Inputs](https://itp.nyu.edu/physcomp/lessons/microcontrollers/analog-input/)
* [Analog to Digital Conversion](https://learn.sparkfun.com/tutorials/analog-to-digital-conversion)

### Analog
  * infinite resolution

### Digital
  * finite resolution

N is resolution in bits

Least Significant Bit is the smalled change in voltage we're able to detect

Vref is our reference voltage (3.3v in most cases)

```math
 LSB = (Vref)/2^N
 ```
```math
 Vin = Code * LSB
 ```
```math
 Code = Vin/LSB
 ```


If we have a 4 bit ADC and an input voltage of 2.734 volts
```math
LSB = 3.3v/2^4 = 0.206v
```
```math
Code = 2.734/0.206v = 13
```
```math
Output code is 13
```

If we have a 10 bit ADC and an input voltage of 2.734 volts
```math
LSB = 3.3v/2^10 = 0.00322v
```
```math
Code = 2.734/0.00322v = 849
```
```math
Output code is 849
```

If we have a 24 bit ADC and an input voltage of 2.734 volts
```math
LSB = 3.3v/2^24 = 0.000000197v
```
```math
Code = 2.734/0.000000197v = 13899692
```
```math
Output code is 13899692
```

### What is the output code of a 16 bit ADC?
### What about a 24 bit ADC?
### How much does the Code change with a 4bit ADC if the voltage changes by 0.001v?
### How much does the Code change with a 24 bit ADC if the voltage changes by 0.001v?
