# So, where do you go from here?

Here is a collection of links to resources, ideas, and inspirations. Please dive into them at your leisure, each one is a gateway into more and more information. Some require more background knowledge to fully understand and appreciate, but you can get there. Go find some rabbit holes to go down, it's worth it!

## Electronics Resources
### Basics
* [ITP, Physical Computing](https://itp.nyu.edu/physcomp/)
  * ITP, short for the Interactive Telecommunications Program, is a creative technology Masters program at NYU in New York City. Physical Computing is what we have been doing, using microcontrollers to interact with the world. We've been doing less interaction, and more measuring, but it falls in the same catagory, bridging the physical world and the computer world. This is the Physical Computing class website and is a great resource for foundational knowledge of electronics, programing, and building.
* [ITP, Intro to PCB Fabrication](http://intro2pcbfab.com/)
  * This is a class at ITP which teaches simple Printed Circuit Board Fabrication techniques. The website links back to a lot of Physical Computing resources, but has some unique gems of useful information too.
* [High Low Tech (MIT)](http://highlowtech.org/)
  * Is a part of the Media Lab at MIT in Boston. The Media Lab is another creative technology program and High Low Tech has documentation on integrating electronics and circuits into soft media such as Paper and Cloth. They have some tutorials and some profolio pieces, some good links, and some nice pictures.
* [Sparkfun Education](https://learn.sparkfun.com/)
  * This is a the best resource online for basic electronics, sensors, microcontrollers, and related stuff. They have blog posts, tutorials, and videos that explain basic and complex concepts in a very clear and thoughtful way. If you learn everything here, you will be very smart.
* [Adafruit Education](https://learn.adafruit.com/)
  * This is a similar education branch at Adafruit, which is an electronics supplier with a similar market to Sparkfun. They focus more on the art/craft side of things, with good tutorials on soft circuits. Their resources on LED based projects are especially good, and they are the designers of the NeoPixel, a multi-color programmable LED platform which is very cool.

### Going Further

When you want to dive deeper into the world of Electronics, Programming, and hardware in general start digging here. There is some amazing depth here if you choose to dig. Some digging is required though.

* [EEV Blog](http://www.eevblog.com/)
  * The EEV Blog is a brainchild of this guy, Dave Jones, who is a kooky austrailian electrical engineer who likes blowing stuff up and taking things apart. There is a bit of pretty mild profanity and lots of hilarious electronics nerd t-shirts.
  * [Youtube Channel](https://www.youtube.com/user/EEVblog) -> This is his Youtube Channel, which is a warehouse of amazing information if you're willing to do some digging. There are A LOT of videos.
  * [How Transistors Work](https://www.youtube.com/watch?v=qUeK7pHe0rI) -> Be sure to check out the entire Fundementals Friday video series. It's top notch.
  * [What is an Op Amp](https://www.youtube.com/watch?v=7FYHt5XviKc)
  * [USB Charger TearDown](https://www.youtube.com/watch?v=wi-b9k-0KfE) -> Dave does really good teardowns of hardware. You'll learn a lot just by watching these and listing to him talk about the stuff inside.
  * [SMD Soldering](https://www.youtube.com/watch?v=b9FC9fAlfQE) -> His soldering tutorials are second to none for information, technique, and quality as well.
* [Felix Rusu](https://www.youtube.com/user/FelixEmman/videos)
  * Felix Rusu is the designer of the Moteino wireless development boards. His youtube channel has really excellent info for doing fabrication and assembly on the cheap. There are some excellent tidbits in here.
* [Electronics.StackExchange](https://electronics.stackexchange.com/)
  * The Electronics Stack Exchange is a message board about electronics problems. Search it, ask for help, gain useful knowledge. You could spend two lifetimes reading everything here and not get through half of it. Run into problems? search here first.
* [Github](https://www.github.com/)
  * Github is where you should put your code, your projects, your websites, your dairy. Learn [how to use it](https://guides.github.com/activities/hello-world/) now and you will be cooler than you were, you'll be more employable. You [should learn about version control as well](https://www.atlassian.com/git/tutorials/what-is-version-control).

### Tools
* [ElectroDroid](https://play.google.com/store/apps/details?id=it.android.demi.elettronica)
  * I use this all the time for electronics calculations and looking up reference values, etc, etc. It's a free android app, and it's awesome.
* [Circuits Simulator](http://lushprojects.com/circuitjs/circuitjs.html)
  * This is an online circuit simulator. Build stuff to test your knowledge of electronic theory. Test ideas and circuits before you buy parts, etc, etc. Very VERY USEFUL
* [Moteino](https://lowpowerlab.com/)
  * Wireless and Open source development platform that is Arduino compatible.
* [The Things Network](https://www.thethingsnetwork.org/)
  * Low power wireless open source communication network for environmental sensors. I havn't used it, but it is HIGH on my priority list to start working with and hacking.


## Science and Ecology

### Citizen Science
* [Project Noah](http://www.projectnoah.org) - report observations about the natural world
* [Zooniverse](https://www.zooniverse.org) - several projects using photography. Can be done from your home!
* [Aurora Watch](http://aurorasaurus.org/) - report aurora sightings, verify tweets about aurora
* [eBird](http://ebird.org/content/ebird/) - report bird sightings
* [iNaturalist](https://www.inaturalist.org/) - report plant and animal sightings, get help with identification
* [Urban Ecology](http://robdunnlab.com/projects/) - biodiversity in your belly button, bugs in your home, and more!
* [Genetics](http://lifescanner.net/) - send in plant or animal samples for genetic identification!

### Volunteering

Fairbanks
* Cold Climate Housing Research Center
* Alaska Songbird Institutue
* Calypso Farm & Ecology Center

Homer
* Center for Alaskan Coastal Studies

Several university professors welcome high school students to intern in their labs. This is a great way to gain research experience and get a head start on figuring out what you want to do in college.

## Communities
* [Public Lab](https://publiclab.org/dashboard)
  * Public Lab is a community where you can learn how to investigate environmental concerns. Using inexpensive DIY techniques, we seek to change how people see the world in environmental, social, and political terms.
* [Gathing of Open Science Hardware](http://openhardware.science/)
  * The Gathering for Open Science Hardware (GOSH) is a diverse, global community working to enhance the sharing of open, scientific technologies.
  * [the Forum](https://forum.openhardware.science/)
* [Open Source Hardware Association](https://www.oshwa.org/)
  * The Open Source Hardware Association aims to be the voice of the open hardware community, ensuring that technological knowledge is accessible to everyone, and encouraging the collaborative development of technology that serves education, environmental sustainability, and human welfare.
